document.addEventListener("DOMContentLoaded", function(event) {
	let checkboxes = document.querySelectorAll("[data-summ]");
	for (let i = 0; i < checkboxes.length; i++) {
		checkboxes[i].addEventListener('change', function() {
			setSumm(this.getAttribute('data-summ'), this.getAttribute('id'));
		});
	}
	setSumm();
});

function setSumm(checkBox, checkboxId) {
	if (checkBox) {
		let checked = document.getElementById(checkboxId).checked;
		if (checkBox == 1 || checkBox == 3) {
			if (checked) {
				document.getElementById(checkboxId + '_homeworks').disabled = false;
				document.getElementById(checkboxId + '_homeworks').checked = true;
			} else {
				document.getElementById(checkboxId + '_homeworks').checked = false;
				document.getElementById(checkboxId + '_homeworks').disabled = true;
			}
		}
	}

	let check1 = document.getElementById('course_1').checked;
	let check2 = document.getElementById('course_1_homeworks').checked;
	let check3 = document.getElementById('course_2').checked;
	let check4 = document.getElementById('course_2_homeworks').checked;

	let summ = 0;

	if (check1 && check2 && check3 && check4) {
		summ = 14000;
	} else {
		if (check1) {
			if (check2) {
				summ += 8000;
			} else {
				summ += 3000;
			}
		}

		if (check3) {
			if (check4) {
				summ += 8000;
			} else {
				summ += 3000;
			}
		}
	}

	let course = (check1 ? '1' : '0') + (check2 ? '1' : '0') + (check3 ? '1' : '0') + (check4 ? '1' : '0');

	document.getElementById('summ').innerHTML = summ;
	document.getElementById('course').value = course;
}